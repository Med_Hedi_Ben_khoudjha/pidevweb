<?php
namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Loisircomment;
use AppBundle\Entity\Loisir;
class LoisircommentRepository extends EntityRepository
{
    /**
     * get post loisircomment
     *
     * @param integer $loisir_id
     *
     * @return array
     */
    public function getPostComments($loisir_id){
        return $this->getEntityManager()
            ->createQuery(
                "SELECT c, u.username
       FROM AppBundle:Loisircomment c
       JOIN c.user u
       WHERE c.loisir = :id"
            )
            ->setParameter('id', $loisir_id)
            ->getArrayResult();
    }
}