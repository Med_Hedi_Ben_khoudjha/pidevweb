<?php
namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Loisir;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
class LoisirRepository extends EntityRepository
{
    /**
     * get all loisirs
     *
     * @return array
     */
    public function findAllPosts()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l
         FROM AppBundle:Loisir l
      
         ORDER BY a.posted_at DESC'
            )
            ->getArrayResult();
    }
    /**
     * get one by id
     *
     * @param integer $id
     *
     * @return array
     */
    public function findOneById($id)
    {
        return $this->getEntityManager()
            ->createQuery(
                "SELECT a, u.username
       FROM AppBundle:Loisir
       a JOIN a.user u
        WHERE a.id = id"
            )
            ->setParameter('id', $id)
            ->getArrayResult();
    }
    /**
     * get one by id
     *
     * @param integer $id
     *
     * @return object or null
     */
    public function findLoisirByid($id)
    {
        try {
            return $this->getEntityManager()
                ->createQuery(
                    "SELECT l
                FROM AppBundle:Loisir
                l WHERE l.id =:id"
                )
                ->setParameter('id', $id)
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
    public function findEntitiesByString($str){
        return $this->getEntityManager()
            ->createQuery(
                'SELECT l
                FROM AppBundle:Loisir l
                WHERE l.title LIKE :str'
            )
            ->setParameter('str', '%'.$str.'%')
            ->getResult();
    }
}