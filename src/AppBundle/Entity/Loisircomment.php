<?php
// src/AppBundle/Entity/User.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
* @ORM\Entity(repositoryClass="AppBundle\Repository\LoisircommentRepository")
* @ORM\Table(name="loisircomment")
* @ORM\HasLifecycleCallbacks()
*/
class Loisircomment
{
/**
* @var string
*
* @ORM\Column(name="content", type="text", unique=false)
*/
private $content;
/**
* @ORM\ManyToOne(targetEntity="AppBundle\Entity\Loisir", inversedBy="comments")
* @ORM\JoinColumn(name="loisir_id", referencedColumnName="id")
*/
private $loisir;
/**
* @ORM\ManyToOne(targetEntity="Agence\userBundle\Entity\membre", inversedBy="comments")
* @ORM\JoinColumn(name="user_id", referencedColumnName="id")
*/
private $user;
/**
* @Assert\DateTime
*/
private $posted_at;
/**
* @ORM\PrePersist
*/
public function setPostedAt()
{
$this->posted_at = new \DateTime('now');
}
/**
* @ORM\Id
* @ORM\Column(type="integer")
* @ORM\GeneratedValue(strategy="AUTO")
*/
protected $id;
/**
* @return mixed
*/
public function getId()
{
return $this->id;
}
/**
* @param mixed $id
*/
public function setId($id)
{
$this->id = $id;
}
/**
* @return string
*/
public function getContent()
{
return $this->content;
}
/**
* @param string $content
*/
public function setContent($content)
{
$this->content = $content;
}
/**
* @return mixed
*/
public function getUser()
{
return $this->user;
}
/**
* @param mixed $user
*/
public function setUser($user)
{
$this->user = $user;
}
/**
* @return mixed
*/
public function getLoisir()
{
return $this->loisir;
}
/**
* @param mixed $post
*/
public function setLoisir($loisir)
{
$this->loisir = $loisir;
}
}