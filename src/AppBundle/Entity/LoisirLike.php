<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
/**
 * LoisirLike
 *
 * @ORM\Table(name="loisirlike")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LoisirLikeRepository")
 */
class LoisirLike
{


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Loisir", inversedBy="likes")
     */
    private $loisir;
    /**
     * @ORM\ManyToOne(targetEntity="Agence\userBundle\Entity\membre", inversedBy="likes")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLoisir()
    {
        return $this->loisir;
    }

    /**
     * @param mixed $loisir
     */
    public function setLoisir($loisir)
    {
        $this->loisir = $loisir;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}