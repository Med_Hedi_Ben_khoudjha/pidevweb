<?php

namespace PdfBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PdfController extends Controller
{ public function pdfAction($id)
{
    $user = $this->get('security.token_storage')->getToken()->getUser();


    $em = $this->getDoctrine()->getManager();
    $rec=$em->getRepository('TransportBundle:transport')->find($id);




    $html = $this->renderView('PdfBundle:Pdf:pdf.html.twig',array('transport'=>$rec));



    $filename = sprintf('Transport-%s.pdf', date('Y-m-d'));

    return new Response(
        $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
        200,
        [
            'Content-Type'        => 'application/pdf',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $filename), 'encoding' => 'utf-8',
        ]
    );
    return $this->redirectToRoute('khalil_list_transport2');


}









}