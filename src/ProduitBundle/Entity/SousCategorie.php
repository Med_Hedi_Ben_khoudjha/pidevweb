<?php

namespace ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SousCategorie
 *
 * @ORM\Table(name="sous_categorie")
 * @ORM\Entity(repositoryClass="ProduitBundle\Repository\SousCategorieRepository")
 */
class SousCategorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomSousCategorie", type="string", length=255)
     */
    private $nomSousCategorie;

    /**
     * @var string
     *
     * @ORM\Column(name="referenceSousCategorie", type="string", length=255)
     */
    private $referenceSousCategorie;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomSousCategorie
     *
     * @param string $nomSousCategorie
     *
     * @return SousCategorie
     */
    public function setNomSousCategorie($nomSousCategorie)
    {
        $this->nomSousCategorie = $nomSousCategorie;

        return $this;
    }

    /**
     * Get nomSousCategorie
     *
     * @return string
     */
    public function getNomSousCategorie()
    {
        return $this->nomSousCategorie;
    }

    /**
     * Set referenceSousCategorie
     *
     * @param string $referenceSousCategorie
     *
     * @return SousCategorie
     */
    public function setReferenceSousCategorie($referenceSousCategorie)
    {
        $this->referenceSousCategorie = $referenceSousCategorie;

        return $this;
    }

    /**
     * Get referenceSousCategorie
     *
     * @return string
     */
    public function getReferenceSousCategorie()
    {
        return $this->referenceSousCategorie;
    }



    /**
     * @ORM\OneToMany(targetEntity="ProduitBundle\Entity\Produit", mappedBy="souscategorie")
     * @ORM\JoinColumn(name="produit_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $produit;

    /**
     * Set produit
     *
     * @param \ProduitBundle\Entity\Produit $produit
     *
     * @return sousCategorie
     */
    public function setProduit(\ProduitBundle\Entity\Produit $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \ProduitBundle\Entity\Produit
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
         * @ORM\ManyToOne(targetEntity="ProduitBundle\Entity\Categorie")
     * @ORM\JoinColumn(name="categorie_id", referencedColumnName="id")
     */
    private $categorie;

    /**
     * Set categorie
     *
     * @param \ProduitBundle\Entity\Categorie $categorie
     *
     * @return sousCategorie
     */
    public function setCategorie(\ProduitBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \ProduitBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    public function __toString()
    {
       return $this->nomSousCategorie;
    }


}

