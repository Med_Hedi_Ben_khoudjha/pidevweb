<?php

namespace ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Agence\userBundle\Entity\membre;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="ProduitBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomProduit", type="string", length=255)
     */
    private $nomProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="photoProduit", type="string", length=255)
     *@Assert\File(
     *     maxSize = "2M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Le fichier choisi ne correspond pas à un fichier valide",
     *     notFoundMessage = "Le fichier n'a pas été trouvé sur le disque",
     *     uploadErrorMessage = "Erreur dans l'upload du fichier"
     * )
     */
    private $photoProduit;

    /**
     * @var \string
     *
     * @ORM\Column(name="dateProduit",  type="string", length=255)
     */
    private $dateProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var float
     *
     * @ORM\Column(name="prixVente", type="float")
     */
    private $prixVente;

    /**
     * @var float
     *
     * @ORM\Column(name="prixAchat", type="float")
     */
    private $prixAchat;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set dateProduit
     *
     * @param \DateTime $dateProduit
     *
     * @return Produit
     */
    public function setDateProduit($dateProduit)
    {
        $this->dateProduit = $dateProduit;

        return $this;
    }

    /**
     * Get dateProduit
     *
     * @return \DateTime
     */
    public function getDateProduit()
    {
        return $this->dateProduit;
    }

    /**
     * Set nomProduit
     *
     * @param string $nomProduit
     *
     * @return Produit
     */
    public function setNomProduit($nomProduit)
    {
        $this->nomProduit = $nomProduit;

        return $this;
    }

    /**
     * Get nomProduit
     *
     * @return string
     */
    public function getNomProduit()
    {
        return $this->nomProduit;
    }


    /**
     * Set photoProduit
     *
     * @param string $photoProduit
     *
     * @return Produit
     */
    public function setPhotoProduit($photoProduit)
    {
        $this->photoProduit = $photoProduit;

        return $this;
    }

    /**
     * Get photoProduit
     *
     * @return string
     */
    public function getPhotoProduit()
    {
        return $this->photoProduit;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Produit
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set prixVente
     *
     * @param float $prixVente
     *
     * @return Produit
     */
    public function setPrixVente($prixVente)
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    /**
     * Get prixVente
     *
     * @return float
     */
    public function getPrixVente()
    {
        return $this->prixVente;
    }

    /**
     * Set prixAchat
     *
     * @param float $prixAchat
     *
     * @return Produit
     */
    public function setPrixAchat($prixAchat)
    {
        $this->prixAchat = $prixAchat;

        return $this;
    }

    /**
     * Get prixAchat
     *
     * @return float
     */
    public function getPrixAchat()
    {
        return $this->prixAchat;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Produit
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }


    /**
     * @ORM\ManyToOne(targetEntity="SousCategorie")
     * @ORM\JoinColumn(name="Souscategorie_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $souscategorie;

    /**
     * @return int
     */
    public function getSousCategorie()
    {
        return $this->souscategorie;
    }

    /**
     * @param int $souscategorie
     */
    public function setSousCategorie($souscategorie)
    {
        $this->souscategorie = $souscategorie;

    }


    /**
     * @ORM\OneToMany(targetEntity="Rating", mappedBy="produit")
     * @ORM\JoinColumn(name="rating_id", referencedColumnName="id")
     */
    private $rating;

    /**
     * Set rating
     *
     * @param \ProduitBundle\Entity\Rating $rating
     *
     * @return produit
     */
    public function setRating(Rating $rating = null)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return \ProduitBundle\Entity\Rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    private $file;

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }





}

