<?php

namespace ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="ProduitBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomCategorie", type="string", length=255)
     */
    private $nomCategorie;

    /**
     * @var string
     *
     * @ORM\Column(name="referenceCategorie", type="string", length=255)
     */
    private $referenceCategorie;

    /**
     *

     */
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCategorie
     *
     * @param string $nomCategorie
     *
     * @return Categorie
     */
    public function setNomCategorie($nomCategorie)
    {
        $this->nomCategorie = $nomCategorie;

        return $this;
    }

    /**
     * Get nomCategorie
     *
     * @return string
     */
    public function getNomCategorie()
    {
        return $this->nomCategorie;
    }

    /**
     * Set referenceCategorie
     *
     * @param string $referenceCategorie
     *
     * @return Categorie
     */
    public function setReferenceCategorie($referenceCategorie)
    {
        $this->referenceCategorie = $referenceCategorie;

        return $this;
    }

    /**
     * Get referenceCategorie
     *
     * @return string
     */
    public function getReferenceCategorie()
    {
        return $this->referenceCategorie;
    }

    /**
     * @ORM\OneToMany(targetEntity="SousCategorie", mappedBy="categorie")
     * @ORM\JoinColumn(name="Souscategorie_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $souscategorie;

    /**
     * @return int
     */
    public function getSousCategorie()
    {
        return $this->souscategorie;
    }

    /**
     * @param int $souscategorie
     */
    public function setSousCategorie($souscategorie)
    {
        $this->souscategorie = $souscategorie;

    }

    public function __toString()
    {
        return $this->nomCategorie;
    }


}

