<?php

namespace ProduitBundle\Controller;

use ProduitBundle\Entity\Categorie;
use ProduitBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * Produit controller.
 *
 */
class ProduitController extends Controller
{
    /**
     * Lists all produit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('ProduitBundle:Produit')->findAll();

        return $this->render('produit/ListeProduitsBack.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * Creates a new produit entity.
     *
     */
    public function newAction(Request $request)
    {
        $produit = new Produit();
        $form = $this->createForm('ProduitBundle\Form\ProduitType', $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var UploadedFile $file
             */
            if($produit->getPrixAchat()>0 &&$produit->getPrixVente()>0&&$produit->getQuantite()>0) {
                $file = $produit->getPhotoProduit();
                $fileName = md5(uniqid()) . '.jpg';
                try {
                    $file->move(
                        //il 5altaa parametre hetha met3adech must be defined
                        $this->getParameter('photo_produit'),
                        $fileName
                    );
                } catch (FileException $e) {

                }
                $produit->setDateProduit(date("Y/m/d"));

                $produit->setPhotoProduit($fileName);
                $em = $this->getDoctrine()->getManager();
                $em->persist($produit);
                $em->flush();
                echo '<script type="text/javascript">' . 'alert("Ajout Avec Succes ");' . '</script>';

                return $this->redirectToRoute('ListeProduitsBack', array('id' => $produit->getId()));
            }
            else
            {
                echo '<script type="text/javascript">' . 'alert("Erreur : Date Ou Valeur Invalide ");' . '</script>';
            }
        }

        return $this->render('@Produit/AjouterProduit.html.twig', array(
            'produit' => $produit,
            'form_add' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a produit entity.
     *
     */
    public function showAction(Produit $produit)
    {
        $deleteForm = $this->createDeleteForm($produit);

        return $this->render('produit/show.html.twig', array(
            'produit' => $produit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing produit entity.
     *
     */
    public function editAction(Request $request, $id)
    { /*
        $deleteForm = $this->createDeleteForm($produit);
        $editForm = $this->createForm('ProduitBundle\Form\ModifierProduitType', $produit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('ListeProduitsBack');
        }

      return $this->render("@Produit/ModifierProduit.html.twig", array(
            'produit' => $produit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        )); */
        $em=$this->getDoctrine()->getManager();
        $produit=$em->getRepository(Produit::class)->find($id);
        if ($request->isMethod("POST"))
        {
            $produit->setNomProduit($request->get("nomProduit"));
            $produit->setQuantite($request->get("quantite"));
            $produit->setPrixVente($request->get("prixVente"));
            $produit->setPrixAchat($request->get("prixAchat"));
            $em->flush();
            return $this->redirectToRoute("ListeProduitsBack");
        }
        return $this->render("@Produit\ModifierProduit.html.twig",array("produit"=>$produit));
    }

    /**
     * Deletes a produit entity.
     *
     */
    public function deleteAction(Request $request, Produit $produit)
    {
        $form = $this->createDeleteForm($produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
        }

        return $this->redirectToRoute('produit_index');
    }

    /**
     * Creates a form to delete a produit entity.
     *
     * @param Produit $produit The produit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produit $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produit_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }


    public function  wishlistAction($id)
    {
        $session= $this->get('session');
        if (!$session->has('wishlist')) $session->set('wishlist',array());
        $panier=$session->get('wishlist');
        if(array_key_exists($id,$panier)){
            $panier[$id]=$panier[$id]+1;
        }else $panier[$id]=1;
        $session->set('wishlist',$panier);


        if (!$session->has('wishlist')) $session->set('wishlist',array());

        return $this->redirectToRoute("displayproduit");
    }


    public function getWishlistAction(){
        $em=$this->getDoctrine()->getManager();

        $session = $this->get('session');
        $wishlist = $session->get('wishlist');
        if (!$session->has('wishlist')) $session->set('wishlist', array());

        $produits = $em->getRepository(Produit::class)->findArray(array_keys($session->get('wishlist')));

        $categories=$this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render("@Produit/swishlistproduit.html.twig",array('produits'=>$produits,'categorieF'=>$categories));
    }

    public function RemoveOneFromWishlistAction($id)
    {
        $session= $this->get('session');
        if (!$session->has('wishlist')) $session->set('wishlist',array());
        $panier=$session->get('wishlist');
        if(array_key_exists($id,$panier)) {
            $panier[$id] -= 1;
            if($panier[$id]==0)
                unset($panier[$id]);
        }
        $session->set('wishlist',$panier);
        $em=$this->getDoctrine()->getManager();
        $produits=$em->getRepository(Produit::class)->findArray(array_keys($session->get('wishlist')));
        return $this->redirectToRoute('getWishlist');
    }

    public function payerAction($id)
    {

        $produit=$this->getDoctrine()->getRepository(produit::class)->find($id);

        return $this->render('@Produit/payer.html.twig',['entity'=>$produit]);
    }
}
