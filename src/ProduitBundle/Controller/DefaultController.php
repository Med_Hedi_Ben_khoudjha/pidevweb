<?php

namespace ProduitBundle\Controller;

use Agence\userBundle\Entity\membre;
use ProduitBundle\Entity\Categorie;
use ProduitBundle\Entity\Rating;
use ProduitBundle\Entity\SousCategorie;
use ProduitBundle\Entity\WishList;
use ProduitBundle\ProduitBundle;
use Symfony\Component\HttpFoundation\Request;
use ProduitBundle\Entity\Produit;
use ProduitBundle\Form\ProduitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProduitBundle:Default:index.html.twig');
    }


    public function displayproduitAction()
    {  $produits=$this->getDoctrine()->getRepository(Produit::class)->findAll();
        $categories=$this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render("@Produit/displayproduit.html.twig",array('produits'=>$produits,'categorieF'=>$categories));
    }

    public function displayProductByCatAction($categorie_id){
        // $categorie=$this->getDoctrine()->getRepository(Categorie::class)->find($id_cat);
        $categories=$this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $products = $this->getDoctrine()->getRepository(Produit::class)->findAll();
        $prod=array();
        foreach ($products as $p){
            if ($p->getSousCategorie()->getCategorie()->getId()==$categorie_id){
                array_push($prod,$p);
            }
        }
        return $this->render("@Produit/displayproduit.html.twig",array('produits'=>$prod,'categorieF'=>$categories));
    }

        public function RelatedItemsAction(Request $request)
        {  $souscategorie=$this->getSousCategorie();
            //$categorie=$this->getDoctrine()->getRepository(Categorie::class)->find();
            $categories=$this->getDoctrine()->getRepository("ProduitBundle:Produit")->ProduitsBySousCategory($souscategorie);
            return $this->render("@Produit/DetailsProduit.html.twig",array('categorie'=>$categories));
        }




    public function templateShopAction()
    { $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render("@Produit/displayproduit.html.twig",array('categorie'=>$categories));
    }




    public function DetailsProduitAction(Request $request,$usr)
    {$produits=$this->getDoctrine()->getRepository(Produit::class)->findAll();
        $id=$request->get('id');
        $products = $this->getDoctrine()->getRepository(Produit::class)->find($id);
        $souscategorie=$products->getSousCategorie();
        //$categorie=$this->getDoctrine()->getRepository(Categorie::class)->find();
        $categories=$this->getDoctrine()->getManager()->getRepository("ProduitBundle:Produit")->ProduitsBySousCategory($souscategorie);


        $nomProduit=$request->get('nomProduit');
        $prixVente=$request->get('prixVente');
        $photoProduit=$request->get('photoProduit');
        $quantite=$request->get('quantite');

        $rating = new Rating();
        $form = $this->createForm('ProduitBundle\Form\RatingType', $rating);
        $form->handleRequest($request);
        $user = $this->getDoctrine()->getRepository(membre::class)->find($usr);

        if ($form->isSubmitted() && $form->isValid()) {
            $rating->setCount(2);
            $rating->setTotalCount(2);
            $rating->setProduit($products);
            $rating->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($rating);
            $em->flush();


        }
        $state=true;
        foreach ($products->getRating() as $ra){
            if ($ra->getUser()->getId()== $user->getId()){
                $state=false;
                break;
            }
        }
        return $this->render("@Produit/DetailsProduit.html.twig",array('state'=>$state,'id'=>$id,'nomProduit'=>$nomProduit,'prixVente'=>$prixVente,'photoProduit'=>$photoProduit,'quantite'=>$quantite,'produit'=>$products,'ff'=>$form->createView(),'categorie'=>$categories,'produits'=>$produits));
    }



    public function ListeProduitsBackAction()
    {
        $connexion=$this->getDoctrine()->getManager();
        $produit=$connexion->getRepository("ProduitBundle:Produit")->findAll();
        return $this->render('@Produit/ListeProduitsBack.html.twig',array("produits"=>$produit));

    }

    public function ListeCategoriesBackAction()
    {
        $connexion=$this->getDoctrine();
        $categorie=$connexion->getRepository("ProduitBundle:Categorie")->findAll();
        return $this->render('@Produit/ListeCategoriesBack.html.twig',array("categorie"=>$categorie));

    }

    public function ListeCategoriesFrontAction()
    {
        $connexion=$this->getDoctrine();
        $categorieF=$connexion->getRepository("ProduitBundle:Categorie")->findAll();
        return $this->render('@Produit/ListeCategoriesFront.html.twig',array("categorieF"=>$categorieF));

    }




    public function ListeSousCategoriesBackAction()
    {
        $connexion=$this->getDoctrine();
        $souscategorie=$connexion->getRepository("ProduitBundle:SousCategorie")->findAll();
        return $this->render('@Produit/ListeSousCategoriesBack.html.twig',array("souscategorie"=>$souscategorie));

    }


    public function AjouterCategorieAction(Request $request )
    {
        $categorie = new Categorie();

        if ($request->isMethod("POST"))
        {
            $em1=$this->getDoctrine()->getManager();
            $categorie->setNomCategorie($request->get("nomCategorie"));
            $categorie->setReferenceCategorie($request->get("referenceCategorie"));



            $em=$this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();
            return $this->redirectToRoute("AjouterCategorie");


        }

        return $this->render("@Produit/AjouterCategorie.html.twig");


    }


    public function AjouterSousCategorieAction(Request $request )
    {
        $souscategorie = new SousCategorie();

        if ($request->isMethod("POST"))
        {
            $em=$this->getDoctrine()->getManager();
            $souscategorie->setNomSousCategorie($request->get("nomSousCategorie"));
            $souscategorie->setReferenceSousCategorie($request->get("referenceSousCategorie"));
            $catg = $em->getRepository(Categorie::class)->find($request->get("categorie"));
            $souscategorie->setCategorie($catg);

            $em=$this->getDoctrine()->getManager();
            $em->persist($souscategorie);
            $em->flush();
            return $this->redirectToRoute("AjouterSousCategorie");


        }
        $connexion=$this->getDoctrine();
        $categories=$connexion->getRepository("ProduitBundle:Categorie")->findAll();
        return $this->render( "@Produit/AjouterSousCategorie.html.twig",array("categorie"=>$categories));



    }



    public function AjouterProduitAction(Request $request )
    {
        $produit = new Produit();

        if ($request->isMethod("POST"))
        {
            $em1=$this->getDoctrine()->getManager();
            $produit->setNomProduit($request->get("nomProduit"));
            $produit->setQuantite($request->get("quantite"));
            $produit->setReference($request->get("reference"));
            $produit->setPrixAchat($request->get("prixAchat"));
            $produit->setPrixVente($request->get("prixVente"));
            $produit->setPhotoProduit($request->get("photoProduit"));
            $produit->setDateProduit(new \DateTime($request->get("dateProduit")));
            $catg = $em1->getRepository(Categorie::class)->find($request->get("categorie"));
            $produit->setCategorie($catg);
            $em1->persist($produit);
            $em1->flush();
            return $this->redirectToRoute("AjouterProduit");


        }
        $connexion=$this->getDoctrine();
        $categories=$connexion->getRepository("ProduitBundle:Categorie")->findAll();
        return $this->render("@Produit/AjouterProduit.html.twig",array("categories"=>$categories));


    }






    public function ModifierProduitAction(Request $request , $id)
    {

        $em=$this->getDoctrine()->getManager();
        $produit=$em->getRepository(Produit::class)->find($id);
        if ($request->isMethod("POST"))
        {
         $produit->setNomProduit($request->get("nomProduit"));
            $produit->setQuantite($request->get("quantite"));
            $produit->setPrixVente($request->get("prixVente"));
            $produit->setPrixAchat($request->get("prixAchat"));
            if($produit->getPrixAchat()>0 && $produit->getPrixVente()>0 && $produit->getQuantite()>0)
            {$em->flush();
            echo '<script type="text/javascript">' . 'alert("Ajout Avec Succes ");' . '</script>';
            return $this->redirectToRoute("ListeProduitsBack");
        }
            else
            {
                echo '<script type="text/javascript">' . 'alert("Erreur : Date Ou Valeur Invalide ");' . '</script>';
            }
        return $this->render("@Produit\ModifierProduit.html.twig",array("produit"=>$produit));}

    }



    public function ModifierCategorieAction(Request $request , $id)
    {

        $em=$this->getDoctrine()->getManager();
        $categorie=$em->getRepository(Categorie::class)->find($id);
        if ($request->isMethod("POST"))
        {
            $categorie->setNomCategorie($request->get("nomCategorie"));

            $em->flush();
            return $this->redirectToRoute("ListeCategoriesBack");
        }
        return $this->render("@Produit\ModifierCategorie.html.twig",array("categorie"=>$categorie));
    }

    public function ModifierSousCategorieAction(Request $request , $id)
    {

        $em=$this->getDoctrine()->getManager();
        $souscategorie=$em->getRepository(SousCategorie::class)->find($id);
        if ($request->isMethod("POST"))
        {
            $souscategorie->setNomSousCategorie($request->get("nomSousCategorie"));

            $em->flush();
            return $this->redirectToRoute("ListeSousCategoriesBack");
        }
        return $this->render("@Produit\ModifierSousCategorie.html.twig",array("souscategorie"=>$souscategorie));
    }


    public function SupprimerProduitAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $produit=$em->getRepository(Produit::class)->find($id);
        $em->remove($produit);
        $em->flush();
        return $this->redirectToRoute("ListeProduitsBack");
    }


    public function SupprimerCategorieAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $categorie=$em->getRepository(Categorie::class)->find($id);
        $em->remove($categorie);
        $em->flush();
        return $this->redirectToRoute("ListeCategoriesBack");
    }

    public function SupprimerSousCategorieAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $souscategorie=$em->getRepository(SousCategorie::class)->find($id);
        $em->remove($souscategorie);
        $em->flush();
        return $this->redirectToRoute("ListeSousCategoriesBack");
    }



    /*public function ListeProduitsByCategorieAction(Request $request,$categorie)
    {

        $em=$this->getDoctrine()->getManager();
        //$cat = $em->getRepository(Categorie::class)->find($request->get("categorie"));
        $categories=$em->getRepository("ProduitBundle:Produit")->ProduitsByCategory($categorie);
        return ($this->render("@Produit\displayproduit.html.twig",array("categories",$categories)));
    }*/


















    public function AjoutProduitsAction(Request $request)
    {
        $produit=new Produit();
        $form=$this->createForm(ProduitType::class,$produit);
        $form->handleRequest($request);
        if($form->isValid())
        {

            $em=$this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();
            return $this->redirectToRoute("AjoutProduits");
        }
        return $this->render("@Produit/AjoutProduits.html.twig",array('form_AjoutProduit'=>$form->createView()));}




    public function displayAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Categorie::class)->findBy(array(), array('id' => 'ASC'),4);

        foreach ($categories as $categorie)
        {
            $categorieid = $categorie->getId();
            $suggestions[$categorieid] = $em->getRepository(Produit::class)->RandomProducts(4,$categorieid);
        }

        return $this->render("@Produit\Reco.html.twig", array(

            'suggestions' => $suggestions,
            'categories' => $categories));
    }



    /*
     * $em = $this->getDoctrine()->getManager();
            $produits = $em->getRepository(Produit::class)->findAll();

            $normalizer = new ObjectNormalizer();
            $normalizer->setCircularReferenceLimit(10);


            $serializer = new Serializer([$normalizer]);
            $formatted = $serializer->normalize($produits);
            return new JsonResponse($formatted);
     * */



    public function showWishListApiAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $wishList = $em->getRepository(WishList::class)->find($id);

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });



        $serializer = new Serializer([$normalizer]);
        $formatted = $serializer->normalize($wishList->getProduits());
        return new JsonResponse($formatted);
    }

    public function showApiAction()
    {
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository(Produit::class)->findAll();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });



        $serializer = new Serializer([$normalizer]);
        $formatted = $serializer->normalize($produits);
        return new JsonResponse($formatted);
    }

    public function addApiAction($id,$list_id)
    {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository(Produit::class)->find($id);
        $wishList = $em->getRepository(WishList::class)->find($list_id);
        $wishList->addProduit($produit);
        $em->persist($wishList);
        $em->flush();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer([$normalizer]);
        $formatted = $serializer->normalize($wishList);
        return new JsonResponse($formatted);
    }

    public function deleteApiAction($id,$list_id)
    {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository(Produit::class)->find($id);
        $wishList = $em->getRepository(WishList::class)->find($list_id);
        $wishList->removeProduit($produit);
        $em->persist($wishList);
        $em->flush();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer([$normalizer]);
        $formatted = $serializer->normalize($wishList);
        return new JsonResponse($formatted);
    }






}
