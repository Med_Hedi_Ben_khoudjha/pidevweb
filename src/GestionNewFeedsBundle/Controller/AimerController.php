<?php

namespace GestionNewFeedsBundle\Controller;

use GestionNewFeedsBundle\Entity\Aimer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Aimer controller.
 *
 */
class AimerController extends Controller
{
    /**
     * Lists all aimer entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $aimers = $em->getRepository('GestionNewFeedsBundle:Aimer')->findAll();

        return $this->render('aimer/index.html.twig', array(
            'aimers' => $aimers,
        ));
    }

    /**
     * Creates a new aimer entity.
     *
     */
    public function newAction(Request $request)
    {
        $aimer = new Aimer();
        $form = $this->createForm('GestionNewFeedsBundle\Form\AimerType', $aimer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($aimer);
            $em->flush();

            return $this->redirectToRoute('aimer_show', array('id' => $aimer->getId()));
        }

        return $this->render('aimer/new.html.twig', array(
            'aimer' => $aimer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a aimer entity.
     *
     */
    public function showAction(Aimer $aimer)
    {
        $deleteForm = $this->createDeleteForm($aimer);

        return $this->render('aimer/show.html.twig', array(
            'aimer' => $aimer,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing aimer entity.
     *
     */
    public function editAction(Request $request, Aimer $aimer)
    {
        $deleteForm = $this->createDeleteForm($aimer);
        $editForm = $this->createForm('GestionNewFeedsBundle\Form\AimerType', $aimer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('aimer_edit', array('id' => $aimer->getId()));
        }

        return $this->render('aimer/edit.html.twig', array(
            'aimer' => $aimer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a aimer entity.
     *
     */
    public function deleteAction(Request $request, Aimer $aimer)
    {
        $form = $this->createDeleteForm($aimer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($aimer);
            $em->flush();
        }

        return $this->redirectToRoute('aimer_index');
    }

    /**
     * Creates a form to delete a aimer entity.
     *
     * @param Aimer $aimer The aimer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Aimer $aimer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('aimer_delete', array('id' => $aimer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
