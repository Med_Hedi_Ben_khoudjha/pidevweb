<?php
/**
 * Created by PhpStorm.
 * User: Malek
 * Date: 4/2/2019
 * Time: 2:44 AM
 */

namespace GestionNewFeedsBundle\Controller;


use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use GestionNewFeedsBundle\Entity\Commtaire;
use GestionNewFeedsBundle\Entity\Likes;
use GestionNewFeedsBundle\Entity\Publication;
use function PHPSTORM_META\elementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class GestionController extends Controller
{
    public function indexAction()
    {
        return $this->render('malek.html.twig');
    }
    public function pageAction()
    {
        return $this->render('page.html.twig');
    }
    public function index2Action()
    {
        return $this->render('Front/Front.html.twig');
    }


    /*AJOUT D'UNE PUBLICATION*/
    public function blogAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $publications = $em->getRepository('GestionNewFeedsBundle:Publication')->findAll();

        $dql   = "SELECT a FROM GestionNewFeedsBundle:publication a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );


        $publication = new Publication();
        $form = $this->createForm('GestionNewFeedsBundle\Form\PublicationType', $publication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if($user != 'anon.')
            $publication->setIdMembre($user);
            if($publication->getFile())
            $publication->uploadPicture();
            $em->persist($publication);

            $em->flush();

            return $this->redirectToRoute('blog');
        }

        return $this->render('publication/Blog.html.twig',[
            'publications'=>$publications,
            'form'=>$form->createView(),
            'pagination' => $pagination,
        ]);
    }

    /*affichage d'une publication
    + ajout commentaire*/
    public function ArticleAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Publication::class)->find($id);
        dump($article);

        $editForm = $this->createForm('GestionNewFeedsBundle\Form\PublicationType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($article->getFile() != null)
                $article->uploadPicture();
            $this->getDoctrine()->getManager()->flush();


        }


        $commtaire = new Commtaire();
        $form = $this->createForm('GestionNewFeedsBundle\Form\CommtaireType', $commtaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if($user != 'anon.')
                $commtaire->setIdMembre($user);
            $commtaire->setDate(new \DateTime());
            $commtaire->setIdPublication($article);
            $em->persist($commtaire);
            $em->flush();

        }

        $commentaires = $em->getRepository(Commtaire::class)->findBy(['idPublication'=>$article]);

        if(isset($_POST['idc']) && isset($_POST['msg'])){
            dump($_POST['idc']);
            $c = $em->getRepository(Commtaire::class)->find($_POST['idc']);
            $c->setMessage($_POST['msg']);
            $em->flush();
        }

        $likes = $em->getRepository(Likes::class)->findBy(['idPublication'=>$article]);

        return $this->render('publication/Article.html.twig',[
            'publication'=>$article,
            'form'=>$editForm->createView(),
            'formc'=>$form->createView(),
            'commentaires'=>$commentaires,
            'likes'=>$likes,
        ]);
    }

    public function deleteAction(Request $request, Publication $publication)
    {
            $id = $publication->getId();
            $em = $this->getDoctrine()->getManager();
            $em->remove($publication);
            $em->flush();


        return $this->redirectToRoute('blog');
    }

    public function listAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT a FROM GestionNewFeedsBundle:publication a";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        // parameters to template
        return $this->render('GestionNewFeedsBundle:publication:Blog.html.twig', array('pagination' => $pagination));
    }

    public function statAction()
    {
        $qb = $this->getDoctrine()->getManager();

        $qb = $qb->createQueryBuilder();

        $qb->select('p.titre, count(c)')
            ->from(Publication::class, 'p')
            ->innerjoin(Commtaire::class,'c','WITH','c.idPublication = p')
            ->groupBy('c.idPublication')
        ;

        $data = $qb->getQuery()->getResult();


        $stat = [['Publication', 'Commentaires']];
        foreach ($data as $datum) {
            dump($datum['titre']);
            $stat []= [$datum['titre'],(int)$datum[1]];
        }

        dump($stat);

        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            $stat
        );
        $pieChart->getOptions()->setTitle('Les statistiques des publications les plus commentées');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        dump([['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
        ]);

        return $this->render('GestionNewFeedsBundle:gestion:statistiques.html.twig', array('piechart' => $pieChart));
    }


    public function LikeDislikeAction(){
        $em = $this->getDoctrine()->getManager();
        $publication = $em->getRepository(Publication::class)->find($_POST['publication']);
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $like = $em->getRepository(Likes::class)->findOneBy(['idPublication'=>$publication->getId(),'idMembre'=>$user->getId()]);
        if($like){

            if(($like->getLiked() == false && $_POST['like'] == 'false') or ($like->getLiked() == true && $_POST['like'] == 'true'))
            {
                $em->remove($like);
            }
            else
                $like->setLiked($_POST['like'] == "true");
        }
        else {
            $like = new Likes();
            $like->setIdPublication($publication);
            $like->setIdMembre($user);
            $like->setLiked($_POST['like'] == "true");
            $em->persist($like);
        }
        $em->flush();



        return new JsonResponse(json_encode('success'));
    }



}