<?php

namespace GestionNewFeedsBundle\Controller;

use GestionNewFeedsBundle\Entity\Commtaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Commtaire controller.
 *
 */
class CommtaireController extends Controller
{
    /**
     * Lists all commtaire entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $commtaires = $em->getRepository('GestionNewFeedsBundle:Commtaire')->findAll();

        return $this->render('commtaire/index.html.twig', array(
            'commtaires' => $commtaires,
        ));
    }

    /**
     * Creates a new commtaire entity.
     *
     */
    public function newAction(Request $request)
    {
        $commtaire = new Commtaire();
        $form = $this->createForm('GestionNewFeedsBundle\Form\CommtaireType', $commtaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commtaire);
            $em->flush();

            return $this->redirectToRoute('commtaire_show', array('id' => $commtaire->getId()));
        }

        return $this->render('commtaire/new.html.twig', array(
            'commtaire' => $commtaire,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a commtaire entity.
     *
     */
    public function showAction(Commtaire $commtaire)
    {
        $deleteForm = $this->createDeleteForm($commtaire);

        return $this->render('commtaire/show.html.twig', array(
            'commtaire' => $commtaire,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing commtaire entity.
     *
     */
    public function editAction(Request $request, Commtaire $commtaire)
    {
        $deleteForm = $this->createDeleteForm($commtaire);
        $editForm = $this->createForm('GestionNewFeedsBundle\Form\CommtaireType', $commtaire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('commtaire_edit', array('id' => $commtaire->getId()));
        }

        return $this->render('commtaire/edit.html.twig', array(
            'commtaire' => $commtaire,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a commtaire entity.
     *
     */
    public function deleteAction(Request $request, Commtaire $commtaire)
    {
        $form = $this->createDeleteForm($commtaire);
        $form->handleRequest($request);
        $idpublication = $commtaire->getIdPublication()->getId();

            $em = $this->getDoctrine()->getManager();
            $em->remove($commtaire);
            $em->flush();


        return $this->redirectToRoute('article',['id'=>$idpublication]);
    }

    /**
     * Creates a form to delete a commtaire entity.
     *
     * @param Commtaire $commtaire The commtaire entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Commtaire $commtaire)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commtaire_delete', array('id' => $commtaire->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
