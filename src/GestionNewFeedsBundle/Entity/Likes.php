<?php

namespace GestionNewFeedsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Likes
 *
 * @ORM\Table(name="likes")
 * @ORM\Entity(repositoryClass="GestionNewFeedsBundle\Repository\LikesRepository")
 */
class Likes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="liked", type="boolean", nullable=true)
     */
    private $liked;

    /**
     * @ORM\ManyToOne(targetEntity="Publication")
     * @ORM\JoinColumn(name="id_publication", referencedColumnName="id", onDelete="CASCADE")
     */
    private $idPublication;


    /**
     * @ORM\ManyToOne(targetEntity="Agence\userBundle\Entity\membre")
     * @ORM\JoinColumn(name="idmembre", referencedColumnName="id")
     */
    private $idMembre;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set liked
     *
     * @param boolean $liked
     *
     * @return Likes
     */
    public function setLiked($liked)
    {
        $this->liked = $liked;

        return $this;
    }

    /**
     * Get liked
     *
     * @return bool
     */
    public function getLiked()
    {
        return $this->liked;
    }

    /**
     * Set idPublication
     *
     * @param \GestionNewFeedsBundle\Entity\Publication $idPublication
     *
     * @return Likes
     */
    public function setIdPublication(\GestionNewFeedsBundle\Entity\Publication $idPublication = null)
    {
        $this->idPublication = $idPublication;

        return $this;
    }

    /**
     * Get idPublication
     *
     * @return \GestionNewFeedsBundle\Entity\Publication
     */
    public function getIdPublication()
    {
        return $this->idPublication;
    }

    /**
     * Set idMembre
     *
     * @param \Agence\userBundle\Entity\membre $idMembre
     *
     * @return Likes
     */
    public function setIdMembre( $idMembre )
    {
        $this->idMembre = $idMembre;

        return $this;
    }

    /**
     * Get idMembre
     *
     * @return \Agence\userBundle\Entity\membre
     */
    public function getIdMembre()
    {
        return $this->idMembre;
    }
}
