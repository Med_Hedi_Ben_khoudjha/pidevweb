<?php

namespace GestionNewFeedsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commtaire
 *
 * @ORM\Table(name="commtaire", indexes={@ORM\Index(name="id_publication", columns={"id_publication", "id_membre"}), @ORM\Index(name="FK_PersonkkkOrder", columns={"id_membre"})})
 * @ORM\Entity
 */
class Commtaire
{
    /**
     * @ORM\ManyToOne(targetEntity="Publication")
     * @ORM\JoinColumn(name="id_publication", referencedColumnName="id", onDelete="CASCADE")
     */
    private $idPublication;

    /**
     * @ORM\ManyToOne(targetEntity="Agence\userBundle\Entity\membre")
     * @ORM\JoinColumn(name="id_membre", referencedColumnName="id")
     */
    private $idMembre;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=20000, nullable=false)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="date", length=255, nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set idPublication
     *
     * @param mixed $idPublication
     *
     * @return Commtaire
     */
    public function setIdPublication($idPublication)
    {
        $this->idPublication = $idPublication;

        return $this;
    }

    /**
     * Get idPublication
     *
     * @return mixed
     */
    public function getIdPublication()
    {
        return $this->idPublication;
    }

    /**
     * Set idMembre
     *
     * @param integer $idMembre
     *
     * @return Commtaire
     */
    public function setIdMembre($idMembre)
    {
        $this->idMembre = $idMembre;

        return $this;
    }

    /**
     * Get idMembre
     *
     * @return integer
     */
    public function getIdMembre()
    {
        return $this->idMembre;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Commtaire
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Commtaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
