<?php

namespace TransportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * covoitureur
 *
 * @ORM\Table(name="covoitureur")
 * @ORM\Entity(repositoryClass="TransportBundle\Repository\covoitureurRepository")
 */
class covoitureur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nationalite", type="string", length=255)
     */
    private $nationalite;

    /**
     * @var int
     *
     * @ORM\Column(name="annePremis", type="integer")
     */
    private $annePremis;

    /**
     * @var int
     *
     * @ORM\Column(name="nsvehicule", type="integer")
     */
    private $nsvehicule;

    /**
     * @var int
     *
     * @ORM\Column(name="banis", type="integer",options={"default" = 0})
     */
    private $banis=0;
    /** @ORM\ManyToOne(targetEntity="TransportBundle\Entity\trajet",inversedBy="covoitureur")
     * @ORM\JoinColumn(name="trajet",referencedColumnName="id" ,onDelete="CASCADE")

     */
    protected $trajet;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nationalite
     *
     * @param string $nationalite
     *
     * @return covoitureur
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    /**
     * Get nationalite
     *
     * @return string
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set annePremis
     *
     * @param integer $annePremis
     *
     * @return covoitureur
     */
    public function setAnnePremis($annePremis)
    {
        $this->annePremis = $annePremis;

        return $this;
    }

    /**
     * Get annePremis
     *
     * @return int
     */
    public function getAnnePremis()
    {
        return $this->annePremis;
    }

    /**
     * Set nsvehicule
     *
     * @param integer $nsvehicule
     *
     * @return covoitureur
     */
    public function setNsvehicule($nsvehicule)
    {
        $this->nsvehicule = $nsvehicule;

        return $this;
    }

    /**
     * Get nsvehicule
     *
     * @return int
     */
    public function getNsvehicule()
    {
        return $this->nsvehicule;
    }

    /**
     * Set banis
     *
     * @param integer $banis
     *
     * @return covoitureur
     */
    public function setBanis($banis)
    {
        $this->banis = $banis;

        return $this;
    }

    /**
     * Get banis
     *
     * @return int
     */
    public function getBanis()
    {
        return $this->banis;
    }

    /**
     * @return mixed
     */
    public function getTrajet()
    {
        return $this->trajet;
    }

    /**
     * @param mixed $trajet
     */
    public function setTrajet($trajet)
    {
        $this->trajet = $trajet;
    }
}
