<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 13/04/2019
 * Time: 19:40
 */

namespace TransportBundle\Form;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;





class covoitureurtType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nationalite')
            ->add('annePremis')
            ->add('nsvehicule')
            ->add('trajet',EntityType::class,['class'=>'TransportBundle:trajet',
                'choice_label'=>'nom'])

            ->add('Valider',SubmitType::class);
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TransportBundle\Entity\covoitureur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'TransportBundle_covoitureur';
    }


}