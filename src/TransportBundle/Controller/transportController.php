<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 13/04/2019
 * Time: 19:32
 */

namespace TransportBundle\Controller;




use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use TransportBundle\Entity\trajet;
use TransportBundle\Entity\transport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TransportBundle\Form\trajetType;
use TransportBundle\Form\transportType;

class transportController extends Controller
{
    public function listAction(){
        //Créer une instance de l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        $transport =  $em->getRepository('TransportBundle:transport')
            ->findAll();
        return $this->render('@Transport/transport/list.html.twig'
            ,array(
                "transport"=>$transport
            ));
    }
    public function listFAction(){
        //Créer une instance de l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        $transport =  $em->getRepository('TransportBundle:transport')
            ->findAll();
        return $this->render('@Transport/transport/listFront.html.twig'
            ,array(
                "transport"=>$transport
            ));
    }


    public function deleteAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $transport = $em->getRepository('TransportBundle:transport')
            ->find($id);
        $em->remove($transport);
        $em->flush();
        return $this->redirectToRoute('listeTransport');
    }
    public function addAction(Request $request){
        $transport = new transport();
        $form = $this->createForm(transportType::class,$transport);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            // echo 'suite au clic sur le submit';
            $em = $this->getDoctrine()->getManager();
            $em->persist($transport);
            $em->flush();
            return $this->redirectToRoute('khalil_list_transport');
        }
        return $this->render('@Transport/transport/ajouter.html.twig'
            ,array(
                "form"=>$form->createView()
            ));
    }


    public function updateAction(Request $request, $Id)
    {
        $agence = new transport();
        $em = $this->getDoctrine()->getManager();
        $agence = $em->getRepository('TransportBundle:transport')
            ->find($Id);

        $agence->setCompagnie($agence->getCompagnie());
        $agence->setType($agence->getType());
        $agence->setHoraire($agence->getHoraire());
        $agence->setType($agence->getType());
        $agence->setPrix($agence->getPrix());
        $agence->setTrajet($agence->getTrajet());
        $form=$this->createFormBuilder($agence)->
        add('compagnie',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"compagnie")))->
        add('type',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"type")))->
        add('horaire',TelType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"horaire")))->
        add('prix',TelType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"prix")))
            ->add('trajet',EntityType::class,['class'=>'TransportBundle:trajet','choice_label'=>'nom'])
            ->add('Valider',SubmitType::class)
            ->getForm();
        $form=$form->handleRequest($request);// Récuperation des données

        if($form->isValid() && $form->isSubmitted())// test if our form is valid
        {
            $compagnie=$form['compagnie']->getData();
            $type=$form['type']->getData();
            $horaire=$form['horaire']->getData();
            $prix=$form['prix']->getData();
            $nom=$form['trajet']->getData();

            $em=$this->getDoctrine()->getManager();
            $agence=$em->getRepository(transport::class)->find($Id);

            $agence->setCompagnie($compagnie);
            $agence->setType($type);
            $agence->setHoraire($horaire);
            $agence->setPrix($prix);
            $agence->setTrajet($nom);

            $em->flush();


        }
        return $this->render('@Transport/transport/update.html.twig', array('form'=>$form->createView()
            // ...
        ));

    }



    /*public function updateAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $transport = $em
            ->getRepository('TransportBundle:transport')
            ->find($id);
        $form = $this->createForm(transportType::class,$transport);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            // echo 'suite au clic sur le submit';
            $em = $this->getDoctrine()->getManager();
            $em->persist($transport);
            $em->flush();
            return $this->redirectToRoute('updateTransport');
        }
        return $this->render('@Transport/transport/update.html.twig'
            ,array(
                "Form"=>$form->createView()
            ));
    }*/
    public function ajouterAction(Request $request){
        if($request->isMethod('POST')){
            $compagnie= $request->get('compagnie');
            $type = $request->get('type');
            $horaire = $request->get('horaire');

            $prix = $request->get('prix');
            $transport = new transport();
            $transport->setCompagnie($compagnie);
            $transport->setType($type);
            $transport->setHoraire($horaire);

            $transport->setPrix($prix);

            $em = $this->getDoctrine()->getManager();
            $em->persist($transport);
            $em->flush();
            return $this->redirectToRoute('ajouterTransport');
        }

        return $this->render('@Transport/transport/ajouter.html.twig');
    }


}