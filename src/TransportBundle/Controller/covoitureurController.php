<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 13/04/2019
 * Time: 19:37
 */

namespace TransportBundle\Controller;


use TransportBundle\Entity\covoitureur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TransportBundle\Form\covoitureurtType;

class covoitureurController extends Controller
{

    public function listAction(){
        //Créer une instance de l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        $covoitureur =  $em->getRepository('TransportBundle:covoitureur')
            ->findAll();
        return $this->render('@Transport/covoitureur/listC.html.twig'
            ,array(
                "covoitureur"=>$covoitureur
            ));
    }
    public function listFAction(){
        //Créer une instance de l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        $covoitureur =  $em->getRepository('TransportBundle:covoitureur')
            ->findAll();
        return $this->render('@Transport/covoitureur/listCFront.html.twig'
            ,array(
                "covoitureur"=>$covoitureur
            ));
    }
    public function deleteAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $covoitureur = $em->getRepository('TransportBundle:covoitureur')
            ->find($id);
        $em->remove($covoitureur);
        $em->flush();
        return $this->redirectToRoute('listeCovoitureur');
    }
    public function signaleAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $covoitureur = $em->getRepository('TransportBundle:covoitureur')
            ->find($id);
        $s=$covoitureur->getBanis();
        $s++;
        if($s>2) {
            $em->remove($covoitureur);
            $em->flush();
            return $this->redirectToRoute('listeCovoitureur2');
        }else {
            $em=$this->getDoctrine()->getManager();
            $covoitureur=$em->getRepository(covoitureur::class)->find($id);
            $covoitureur->setBanis($s);
            $em->flush();

            return $this->redirectToRoute('listeCovoitureur2');
        }
    }
    public function addAction(Request $request){
        $covoitureur = new covoitureur();
        $form = $this->createForm(covoitureurtType::class,$covoitureur);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            // echo 'suite au clic sur le submit';
            $em = $this->getDoctrine()->getManager();
            $em->persist($covoitureur);
            $em->flush();

            return $this->redirectToRoute('listeCovoitureur2');
        }
        return $this->render('@Transport/covoitureur/ajouterC.html.twig'
            ,array(
                "form"=>$form->createView()
            ));
    }
    public function updateAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $covoitureur = $em
            ->getRepository('TransportBundle:covoitureur')
            ->find($id);
        $form = $this->createForm(covoitureurtType::class,$covoitureur);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            // echo 'suite au clic sur le submit';
            $em = $this->getDoctrine()->getManager();
            $em->persist($covoitureur);
            $em->flush();
            return $this->redirectToRoute('updateCovoitureur');
        }
        return $this->render('@transport/covoitureur/updateC.html.twig'
            ,array(
                "form"=>$form->createView()
            ));
    }
    public function ajouterAction(Request $request){
        if($request->isMethod('POST')){
            $nationalite= $request->get('nationalite');
            $annepermis = $request->get('annepremis');
            $nsvehicule = $request->get('nsvehicule');


            $covoitureur = new covoitureur();
            $covoitureur->setNationalite($nationalite);
            $covoitureur->setAnnePremis($annepermis);
            $covoitureur->setAnnePremis($nsvehicule);



            $em = $this->getDoctrine()->getManager();
            $em->persist($covoitureur);
            $em->flush();
            return $this->redirectToRoute('ajouterCovoitureur');
        }

        return $this->render('@transport/covoitureur/ajouterC.html.twig');
    }


}