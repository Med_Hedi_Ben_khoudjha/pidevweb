<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 14/04/2019
 * Time: 23:48
 */

namespace TransportBundle\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use TransportBundle\Entity\trajet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TransportBundle\Form\trajetType;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;


class trajetController extends Controller


{
    public function allAction()
    {

        $em = $this->getDoctrine()->getManager();
        $trajet =  $em->getRepository('TransportBundle:trajet')
            ->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($trajet);
        return new JsonResponse($formatted);
    }

    public function listAction(){
        //Créer une instance de l'Entity Manager
        $em = $this->getDoctrine()->getManager();
        $trajet =  $em->getRepository('TransportBundle:trajet')
            ->findAll();
        return $this->render('@Transport/trajet/listT.html.twig'
            ,array(
                "trajet"=>$trajet
            ));
    }
    public function updateAction(Request $request, $Id)
    {
        $trajet = new trajet();
        $em = $this->getDoctrine()->getManager();
        $trajet = $em->getRepository('TransportBundle:trajet')
            ->find($Id);

        $trajet->setPosition($trajet->getPosition());
        $trajet->setDestination($trajet->getDestination());
        $trajet->setDistance($trajet->getDistance());

        $form=$this->createFormBuilder($trajet)->
        add('position',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"compagnie")))->
        add('destination',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"type")))->
        add('distance',TelType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"horaire")))
            ->add('Valider',SubmitType::class)
            ->getForm();
        $form=$form->handleRequest($request);// Récuperation des données

        if($form->isValid() && $form->isSubmitted())// test if our form is valid
        {
            $position=$form['position']->getData();
            $destination=$form['destination']->getData();
            $distance=$form['distance']->getData();

            $nom=$position ." vers " .$destination;

            $em=$this->getDoctrine()->getManager();
            $trajet=$em->getRepository(trajet::class)->find($Id);

            $trajet->setPosition($position);
            $trajet->setDestination($destination);
            $trajet->setDistance($distance);
            $trajet->setNom($nom);


            $em->flush();


        }
        return $this->render('@Transport/trajet/updateT.html.twig', array('form'=>$form->createView()
        ));

    }

        public function deleteAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $trajet = $em->getRepository('TransportBundle:trajet')
            ->find($id);
        $em->remove($trajet);
        $em->flush();
        return $this->redirectToRoute('listeTrajet');
    }
    public function addAction(Request $request)
    {
        $traje = new trajet();
        $form = $this->createForm(trajetType::class, $traje);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // echo 'suite au clic sur le submit';
            $nom = $traje->getPosition() . " vers " . $traje->getDestination();
            $traje->setNom($nom);
            $em = $this->getDoctrine()->getManager();

            $em->persist($traje);
            $em->flush();
            return $this->redirectToRoute('listeTrajet');
        }
        return $this->render('@Transport/trajet/addT.html.twig', array('form'=>$form->createView()
        ));
    }
        public function addFAction(Request $request){
            $traje = new trajet();
            $form = $this->createForm(trajetType::class,$traje);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                // echo 'suite au clic sur le submit';
                $nom=$traje->getPosition()." vers ".$traje->getDestination();
                $traje->setNom($nom);
                $em = $this->getDoctrine()->getManager();

                $em->persist($traje);
                $em->flush();
                return $this->redirectToRoute('addcovoitureur');
            }

            return $this->render('@Transport/trajet/addTF.html.twig'
            ,array(
                "form"=>$form->createView()
            ));
    }
/*    public function updateAction(Request $request){
        $id = $request->get('Id');
        $em = $this->getDoctrine()->getManager();
        $trajet = $em
            ->getRepository('TransportBundle:trajet')
            ->find($id);
        $form = $this->createForm(trajetType::class,$trajet);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            // echo 'suite au clic sur le submit';
            $em = $this->getDoctrine()->getManager();
            $em->persist($trajet);
            $em->flush();
            return $this->redirectToRoute('updateTrajet');
        }
        return $this->render('@Transport/trajet/updateT.html.twig'
            ,array(
                "Form"=>$form->createView()
            ));
    }*/
    public function ajouterAction(Request $request){
        if($request->isMethod('POST')){
            $position= $request->get('position');
            $destination = $request->get('destination');
            $distance = $request->get('distance');


            $trajet = new trajet();
            $trajet->setPosition($position);
            $trajet->setDestination($destination);
            $trajet->setDistance($distance);



            $em = $this->getDoctrine()->getManager();
            $em->persist($trajet);
            $em->flush();
            return $this->redirectToRoute('ajouterTransport');
        }

        return $this->render('@Transport/trajet/ajouterT.html.twig');
    }

}