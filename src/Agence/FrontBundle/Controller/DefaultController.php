<?php

namespace Agence\FrontBundle\Controller;

use Agence\userBundle\Entity\agence;
use Agence\userBundle\Entity\membre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{
    public function indexAction()
    {
//ism name space et bed ism bundle lesé9 ba3tho
        return $this->render('@AgenceFront/Acc/tes.html.twig');
    }

    public function allAction()
    {
        $task=$this->getDoctrine()->getManager()->getRepository('AgenceuserBundle:agence')->findAll();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($task);
        return new JsonResponse($formatted);

//ism name space et bed ism bundle lesé9 ba3tho
    }

    public function loginAction()
    {
        $em=$this->getDoctrine()->getManager();
        $membre=$em->getRepository(membre::class)->findAll();
        //var_dump($caferesto); die;

        $serializer=new Serializer([new ObjectNormalizer()]);

        $formatted=$serializer->normalize($membre);

        return new JsonResponse($formatted);




//ism name space et bed ism bundle lesé9 ba3tho
    }
    public function agenceAction()
    {
        $em=$this->getDoctrine()->getManager();
        $membre=$em->getRepository(agence::class)->findAll();
        //var_dump($caferesto); die;

        $serializer=new Serializer([new ObjectNormalizer()]);

        $formatted=$serializer->normalize($membre);

        return new JsonResponse($formatted);




//ism name space et bed ism bundle lesé9 ba3tho
    }



    public function findAction($id)
    {
        $task=$this->getDoctrine()->getManager()->getRepository('AgenceuserBundle:agence')->find($id);
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($task);
        return new JsonResponse($formatted);

//ism name space et bed ism bundle lesé9 ba3tho
    }

public function newAction(Request $request){
$em=$this->getDoctrine()->getManager();
$agence=new agence();
    $agence->setNom($request->get('nom'));
    $agence->setType($request->get('type'));
    $agence->setLatitude($request->get('latitude'));
    $agence->setLongitude($request->get('longitude'));
    $em->persist($agence);
    $em->flush();
    $serializer=new Serializer([new ObjectNormalizer()]);
    $formatted=$serializer->normalize($agence);
    return new JsonResponse($formatted);


}
    public function newuserAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $membre=new membre();
        $membre->setUsername($request->get('username'));
        $membre->setPassword($request->get('password'));
        $membre->setEmail($request->get('email'));
        $em->persist($membre);
        $em->flush();
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($membre);
        return new JsonResponse($formatted);


    }
    public function deleteAction(Request $request,$id)
    {
        $agence=new agence();
        $em=$this->getDoctrine()->getManager();
        $agence=$em->getRepository(agence::class)->find($id); // Déclaration Entity Manager
        $em->remove($agence);
        $em->flush();
        $this->addFlash('success','Agence Supprimer ');
        $serializer=new Serializer([new ObjectNormalizer()]);
        $formatted=$serializer->normalize($agence);
        return new JsonResponse($formatted);

    }
}
