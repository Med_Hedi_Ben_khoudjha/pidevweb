<?php

namespace Agence\FrontBundle\Controller {

    use Doctrine\DBAL\Types\IntegerType;
    use Agence\userBundle\Entity\offre;
    use Agence\userBundle\Form\offreType;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TelType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\HttpFoundation\Request;
    use Agence\userBundle\Entity\agence;
    use Agence\userBundle\Form\agenceType;


    class OffreController extends Controller
    {


        public function Afficher_OffreAction()
        {

            $offre=$this->getDoctrine()->getRepository(offre::class)->findAll(); // Déclaration Entity Manager

            return $this->render('@AgenceFront/Acc/offre_all.html.twig',array('offre'=>$offre));
        }

        public function newAction(Request $request)
        {

            $ag = new offre(); // objet vide

            $form = $this->createForm(offreType::class,$ag);// Préparation du formulaire
            $form=$form->handleRequest($request);// Récuperation des données
            if($form->isValid())// test if our form is valid
            {
                $em=$this->getDoctrine()->getManager(); // Déclaration Entity Manager
                $ag->setDate(date("Y/m/d"));

                $em->persist($ag); // Persister l'objet modele dans l'ORM
                $em->flush(); // Sauvegarde des données dans la BD
                $this->addFlash('success','Offre ajouter ');
                return $this->redirectToRoute('medmed_offre_all');
            }else
                return $this->render('@AgenceFront/Acc/Ajouter_offre.html.twig', array('form'=>$form->createView()
                    // ...
                ));
        }


        public function deleteAction(Request $request,$id)
        {
            $agence=new offre();
            $em=$this->getDoctrine()->getManager();
            $agence=$em->getRepository(offre::class)->findOneById($id); // Déclaration Entity Manager
            if (!$agence) {
                throw $this->createNotFoundException('No livre found for id '.$id);
            }
            $em->remove($agence);
            $em->flush();
            $this->addFlash('success','Offre Supprimer ');
            return $this->redirectToRoute("medmed_offre_all");

        }

        public function viewoffreAction($id)
        {

            $offre=$this->getDoctrine()->getRepository(offre::class)->find($id);

            return $this->render('@AgenceFront/Acc/View_offre.html.twig',['offre'=>$offre]);
        }

    }
}
