<?php

namespace Agence\adminBundle\Controller;

use Agence\userBundle\Entity\agence;
use Agence\userBundle\Entity\membre;
use Agence\userBundle\Form\agenceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserController extends Controller
{
    public function UserListAction()
    {


        $user=$this->getDoctrine()->getRepository(membre::class)->findAll(); // Déclaration Entity Manager

        return $this->render('@Agenceadmin/template/UserList.html.twig',array('user'=>$user));
    }


}
