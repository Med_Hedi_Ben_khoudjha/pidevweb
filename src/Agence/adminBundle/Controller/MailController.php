<?php

namespace Agence\adminBundle\Controller;

use Agence\userBundle\Entity\mail;
use Agence\userBundle\Entity\membre;
use Agence\userBundle\Entity\user;
use Agence\userBundle\Form\agenceType;
use Agence\userBundle\Form\mailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Tests\Fixtures\ToString;

class MailController extends Controller
{

    public function sendAction(Request $request)
    {

        $mail = new mail(); // objet vide

        $form = $this->createForm(mailType::class,$mail);// Préparation du formulaire
        $form=$form->handleRequest($request);// Récuperation des données
        if($form->isValid()&&$form->isSubmitted())// test if our form is valid
        {
            $subject=$mail->getSubject();
            $email=$mail->getMail();
            $object=$request->get('form')['object'];
            $username="mohamedheidibenkodjha@gmail.com";

            $message=\Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($username)
                ->setTo($email)
                ->setBody($object);
            $this->get('mailer')->send($message);
            $this->addFlash('success','Mail Envoyer ');



        }
            return $this->render('@Agenceadmin/template/mail.html.twig', array('form'=>$form->createView()
                // ...
            ));
    }

    public function forgotAction(Request $request){


        $mail = new mail(); // objet vide
        $form=$this->createFormBuilder($mail)->
        add('mail',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Email")))->

        add('envoyer',SubmitType::class,array('label'=>'Envoyer','attr'=>array('class'=>'btn btn-success')))
            ->getForm();
        $form=$form->handleRequest($request);// Récuperation des données
        if($form->isValid()&&$form->isSubmitted())// test if our form is valid
        {


            $email=$mail->getMail();
            $em=$this->getDoctrine()->getManager();
            $membre=$em->getRepository(membre::class)->findOneBy(array('email' => $email));


            $i=strrpos($membre->getPassword(),  '{' );

            $username="mohamedheidibenkodjha@gmail.com";

            /** @var TYPE_NAME $message */
            $message=\Swift_Message::newInstance()
                ->setSubject("Mot passe")
                ->setFrom($username)
                ->setTo($email)
                ->setBody(substr($membre->getPassword(),0,(int)$i ));
            $this->get('mailer')->send($message);
            return $this->redirectToRoute('fos_user_security_login');



        }
        return $this->render('@Agenceadmin/template/forgot.html.twig', array('form'=>$form->createView()
            // ...
        ));


    }


}
