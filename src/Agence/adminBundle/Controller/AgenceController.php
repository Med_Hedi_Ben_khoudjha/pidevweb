<?php

namespace Agence\adminBundle\Controller;

use Agence\userBundle\Entity\agence;
use Agence\userBundle\Form\agenceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AgenceController extends Controller
{

    public function editAction(Request $request,$id)
    {
        $agence=new agence();
        $agence=$this->getDoctrine()->getRepository(agence::class)->find($id);
        $agence->setlongitude($agence->getlongitude());
        $agence->setlatitude($agence->getlatitude());
        $agence->setInfo($agence->getInfo());
        $agence->setType($agence->getType());
        $agence->setNom($agence->getNom());
        $agence->setTelephone($agence->getTelephone());
        $form=$this->createFormBuilder($agence)->
        add('latitude',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"latitude")))->
        add('longitude',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"longitude")))->
        add('telephone',TelType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Telephone")))->
        add('type',ChoiceType::class,[
            'choices'  => [
                'Choisissez' => null,
                'Restaurent' => 'Restaurent',
                'Hotel' => 'Hotel',
                'Musée' => 'Musée',
                'Café' => 'Café'
            ],
        ])->
        add('info',TextareaType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Information ")))->
        add('nom',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Nom Agence")))->
        add('Modifier',SubmitType::class,array('label'=>'Modifier','attr'=>array('class'=>'btn btn-success ')))
            ->getForm();
        $form=$form->handleRequest($request);// Récuperation des données

        if($form->isValid() && $form->isSubmitted())// test if our form is valid
        {
            $nom=$form['nom']->getData();
            $telephone=$form['telephone']->getData();
            $type=$form['type']->getData();
            $latitude=$form['latitude']->getData();
            $longitude=$form['longitude']->getData();
            $info=$form['info']->getData();

            $em=$this->getDoctrine()->getManager();
            $agence=$em->getRepository(agence::class)->find($id);

            $agence->setlatitude($latitude);
            $agence->setlongitude($longitude);
            $agence->setInfo($info);
            $agence->setType($type);
            $agence->setNom($nom);
            $agence->setTelephone($telephone);

            $em->flush();
            $this->addFlash('success','Agence Modifier ');


        }
        return $this->render('@Agenceadmin/template/edit.html.twig', array('form'=>$form->createView()
            // ...
        ));

    }

    public function createAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //$nom = $request->request->get('exemple');

        $ag = new agence(); // objet vide

        $form = $this->createForm(agenceType::class,$ag);// Préparation du formulaire
        $form=$form->handleRequest($request);// Récuperation des données
        if($form->isValid())// test if our form is valid
        {
            $em=$this->getDoctrine()->getManager(); // Déclaration Entity Manager
            $ag->setMembre($user);
            //$ag->setLatitude($nom);
            $em->persist($ag); // Persister l'objet modele dans l'ORM
            $em->flush(); // Sauvegarde des données dans la BD
            $this->addFlash('success','Agence ajouter ');
            return $this->redirectToRoute('_create');
        }else
            return $this->render('@Agenceadmin/template/agence.html.twig', array('form'=>$form->createView()
                // ...
            ));
    }

    public function viewAction(Request $request,$id)
    {

        $em=$this->getDoctrine()->getRepository(agence::class)->find($id);
        $this->addFlash('success','Agence Afficher ');

        return $this->render('@Agenceadmin/template/view.html.twig',['agence'=>$em]);
    }

    public function deleteAction(Request $request,$id)
    {
        $agence=new agence();
        $em=$this->getDoctrine()->getManager();
        $agence=$em->getRepository(agence::class)->find($id); // Déclaration Entity Manager
        $em->remove($agence);
        $em->flush();
        $this->addFlash('success','Agence Supprimer ');
        return $this->redirectToRoute("_agenge_MJ");

    }

    public function AfficherAction()
    {
        $agence=$this->getDoctrine()->getRepository(agence::class)->findAll(); // Déclaration Entity Manager

        return $this->render('@Agenceadmin/template/agence_mj.html.twig',array('agence'=>$agence));
    }



}
