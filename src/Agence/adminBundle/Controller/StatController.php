<?php

namespace Agence\adminBundle\Controller;

use Agence\userBundle\Entity\user;

use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class StatController extends Controller
{

    public function chartAction()
    {
        // Chart
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('Browser market shares at a specific website in 2010');
        $ob->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true
        ));
        $em=$this->getDoctrine()->getManager();
        $query=$em->createQuery('SELECT COUNT p FROM AgenceuserBundle:agence WHERE p.type LIKE "Restaurent"' );
        $data = array(
            array('Firefox', $query),

        );
        $ob->series(array(array('type' => 'pie','name' => 'Browser share', 'data' => $data)));


        return $this->render('@Agenceadmin/Stat/stat.html.twig', array(
            'chart' => $ob
        ));
    }


    public function PieAction()
    {
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('Nombre des utilisateurs ');
        $ob->plotOptions->pie(array('allowPointSelect' => true, 'cursor' => 'pointer', 'dataLabels' => array('enabled' => false), 'showInLegend' => true));
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT count(et.id)
              FROM AgenceuserBundle:membre et');
        $Administrateur = $query->getSingleScalarResult();
        $et = (int) $Administrateur;
        $em1 = $this->getDoctrine()->getManager();
        $query1 = $em1->createQuery('SELECT count(en.nom)
              FROM AgenceuserBundle:agence en');
        $Client = $query1->getSingleScalarResult();
        $en = (int) $Client;
        $em3 = $this->getDoctrine()->getManager();
        $query3 = $em3->createQuery('SELECT count(en2.libelle)
              FROM AgenceuserBundle:offre en2');
        $Mall = $query3->getSingleScalarResult();
        $en2 = (int) $Mall;

        $data = array(array('Administrateur', $et), array('Client', $en), array('Mall', $en2));
        $ob->series(array(array('type' => 'pie', 'name' => 'Browser share', 'data' => $data)));
        return $this->render('@Agenceadmin/Stat/stat.html.twig', array('chart' => $ob));
    }


}
