<?php

namespace Agence\adminBundle\Controller;

use med\userBundle\Entity\agence;
use med\userBundle\Form\agenceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PayController extends Controller
{
    public function indexAction()
    {
        return $this->render('@Agenceadmin/template/paypal.html.twig');
    }


}
