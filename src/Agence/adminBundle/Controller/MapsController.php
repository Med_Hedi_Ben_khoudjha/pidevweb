<?php

namespace Agence\adminBundle\Controller;

use Agence\userBundle\Entity\agence;
use med\userBundle\Form\agenceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MapsController extends Controller
{


    public function geoAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $caferesto=$em->getRepository(agence::class)->find($id);
        //var_dump($caferesto); die;

        $cgps=$em->getRepository(agence::class)->findBy(array('id'=>$caferesto->getId()));

        return $this->render('@Agenceadmin/template/geo.html.twig', array(
            'caferesto'=>$caferesto,'agence'=>$cgps[0]
        ));
    }



    public function mapsAction()
    {
        $agence=$this->getDoctrine()->getRepository(agence::class)->findAll(); // Déclaration Entity Manager

        return $this->render('@Agenceadmin/template/maps.html.twig',array('agence'=>$agence));
    }
}
