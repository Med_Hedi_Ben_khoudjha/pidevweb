<?php

namespace Agence\adminBundle\Controller;

use Agence\userBundle\Entity\agence;
use Agence\userBundle\Entity\membre;
use Agence\userBundle\Form\agenceType;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use GestionNewFeedsBundle\Entity\Commtaire;
use GestionNewFeedsBundle\Entity\Likes;
use GestionNewFeedsBundle\Entity\Publication;
use function PHPSTORM_META\elementType;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    public function indexAction()
    {

        $qb = $this->getDoctrine()->getManager();

        $qb = $qb->createQueryBuilder();

        $qb->select('p.titre, count(c)')
            ->from(Publication::class, 'p')
            ->innerjoin(Commtaire::class,'c','WITH','c.idPublication = p')
            ->groupBy('c.idPublication')
        ;

        $data = $qb->getQuery()->getResult();


        $stat = [['Publication', 'Commentaires']];
        foreach ($data as $datum) {
            dump($datum['titre']);
            $stat []= [$datum['titre'],(int)$datum[1]];
        }

        dump($stat);

        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            $stat
        );
        $pieChart->getOptions()->setTitle('My Daily Activities');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        dump([['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
        ]);

        $qb = $this->getDoctrine()->getManager();

        $qb = $qb->createQueryBuilder();

        $qb->select('p.titre, count(c)')
            ->from(Publication::class, 'p')
            ->innerjoin(Commtaire::class,'c','WITH','c.idPublication = p')
            ->groupBy('c.idPublication')
        ;

        $data = $qb->getQuery()->getResult();


        $stat = [['Publication', 'Commentaires']];
        foreach ($data as $datum) {
            dump($datum['titre']);
            $stat []= [$datum['titre'],(int)$datum[1]];
        }

        dump($stat);

        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            $stat
        );
        $pieChart->getOptions()->setTitle('les statistiques des publications les plus commentées');
        $pieChart->getOptions()->setHeight(500);
        $pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);

        dump([['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
        ]);

##############################################################################################################
        $qb = $this->getDoctrine()->getManager();

        $qb = $qb->createQueryBuilder();

        $qb->select('p.nom, count(c)')
            ->from(agence::class, 'p')
            ->innerjoin(membre::class,'c','WITH','c.id= p')
        ;

        $data2 = $qb->getQuery()->getResult();


        $stat1 = [['Agence', 'Membre']];
        foreach ($data2 as $datum) {
            dump($datum['nom']);
            $stat1 []= [$datum['nom'],(int)$datum[1]];
        }

        dump($stat1);

        $pieChart1= new PieChart();
        $pieChart1->getData()->setArrayToDataTable(
            $stat1
        );
        $pieChart1->getOptions()->setTitle('Agence');
        $pieChart1->getOptions()->setHeight(500);
        $pieChart1->getOptions()->setWidth(900);
        $pieChart1->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart1->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart1->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart1->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart1->getOptions()->getTitleTextStyle()->setFontSize(20);

        dump([['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
        ]);






        $user=$this->getDoctrine()->getRepository(membre::class)->findAll(); // Déclaration Entity Manager


        return $this->render('@Agenceadmin/template/index_admin.html.twig', array('piechart' => $pieChart,'piechart1' => $pieChart1,'user'=>$user));

    }





    public function agenceAction()
    {
        return $this->render('@Agenceadmin/template/agence.html.twig');
    }
    public function maAction()
    {
        return $this->render('@Agenceadmin/template/gm.html.twig');
    }

    public function agenceMJAction()
    {
        return $this->render('@Agenceadmin/template/agence_mj.html.twig');
    }














    public function lAction()
{
    return $this->render('@medadmin/template/maps.html.twig');
}
}
