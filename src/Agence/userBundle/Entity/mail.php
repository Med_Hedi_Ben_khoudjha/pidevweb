<?php
/**
 * Created by PhpStorm.
 * User: MED
 * Date: 28/03/2019
 * Time: 18:43
 */

namespace Agence\userBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Agence\userBundle\Entity\agence as BaseMessage;


/**
 * @ORM\Entity(repositoryClass="user\Repository\mailRepository")
 * @ORM\Table(options={"collate"="utf8_unicode_ci"})
 * @ORM\Entity
 * @ORM\Table(name="mail")
 */
class mail
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")

     */
    protected $id;


    /**
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    protected $subject;
    /**
     * @ORM\Column(name="mail", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $mail;


    /**
     * @ORM\Column(name="texte", type="string", length=255, nullable=true)
     */
    protected $object;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }


}