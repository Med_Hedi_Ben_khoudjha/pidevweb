<?php

namespace Agence\userBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Agence\userBundle\Entity\agence as BaseMessage;

/**
 * @ORM\Entity(repositoryClass="user\Repository\agenceRepository")
 * @ORM\Table(options={"collate"="utf8_unicode_ci"})
 * @ORM\Entity
 * @ORM\Table(name="agence")
 */
class agence
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")

     */
    protected $id;

    /**
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    protected $adresse;


    /**
     * @ORM\Column(name="telephone	", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    protected $telephone;


    /**
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     *     @Assert\NotBlank
     *    @Assert\Choice(
     *     choices = { "Restaurent", "Hotel","Musée","Café" })
     */
    protected $type;


    /** @ORM\Column(name="info", type="string", length=1055, nullable=true)
     *  @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      )
     */
    protected $info;


    /**
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     * @Assert\NotBlank
v      *      min = 2,
     *      )
     */
    protected $nom;



    /**
     * @ORM\Column(name="longitude", type="float", length=22, nullable=true)
     * @Assert\NotBlank
     */
    protected $longitude;


    /** @ORM\Column(name="latitude", type="float", length=22, nullable=true)
     * @Assert\NotBlank
     */
    protected $latitude;


    /**
     * @ORM\ManyToOne(targetEntity="membre")
     * @ORM\JoinColumn(
     *     name="id_membre",
     *     referencedColumnName="id",
     *     nullable=true
     * )
     */
    public $membre;



    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getMembre()
    {
        return $this->membre;
    }

    /**
     * @param mixed $membre
     */
    public function setMembre($membre)
    {
        $this->membre = $membre;
    }



}
