<?php

namespace Agence\userBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Agence\userBundle\Entity\agence as BaseMessage;

/**
 * @ORM\Entity(repositoryClass="user\Repository\offreRepository")
 * @ORM\Table(options={"collate"="utf8_unicode_ci"})
 * @ORM\Entity
 * @ORM\Table(name="offre")
 */
class offre
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")

     */
    protected $id;






    /**
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     *     @Assert\NotBlank
     *    @Assert\Choice(
     *     choices = { "Restaurent", "Hotel","Musée","Café" })
     */
    protected $type;


    /** @ORM\Column(name="info", type="string", length=1055, nullable=true)
     *  @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      )
     */
    protected $info;


    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      )
     */
    protected $libelle;




    /**
     * @ORM\ManyToOne(targetEntity="agence")
     * @ORM\JoinColumn(
     *     name="id_agence",
     *     referencedColumnName="id",
     *     nullable=false,onDelete="CASCADE"
     * )
     */
    public $id_agence;



    /**
     * @ORM\Column(name="date", type="string",length=255, nullable=true)
     */
    protected $date;

    /**
     * @ORM\Column(name="prix", type="float", nullable=true)
     * @Assert\NotBlank
     */
    protected $prix;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }


    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getIdAgence()
    {
        return $this->id_agence;
    }

    /**
     * @param mixed $id_agence
     */
    public function setIdAgence( $id_agence)
    {
        $this->id_agence = $id_agence;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }



}
