<?php

namespace Agence\userBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AgenceuserBundle extends Bundle
{
    public function getParent()
    {
      return 'FOSUserBundle';
    }
}
