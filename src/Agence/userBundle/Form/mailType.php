<?php

namespace Agence\userBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class mailType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('subject',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Subject")))->
        add('mail',EmailType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Mail")))->
        add('object',TextareaType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Object")))
            ->add('Envoyer',SubmitType::class,array('label'=>'Envoyer','attr'=>array('class'=>'btn btn-primary mt-3')))
    ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Agence\userBundle\Entity\mail'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'Agence_userbundle_mail';
    }


}
