<?php

namespace Agence\userBundle\Form;

use Symfony\Component\Console\Tests\DisabledCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyInfo\Tests\TypeTest;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class agenceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->
        add('telephone',TelType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"72 477 666")))->
        add('type',ChoiceType::class,[
            'choices'  => [
                'Choisissez' => null,
                'Restaurent' => 'Restaurent',
                'Hotel' => 'Hotel',
                'Musée' => 'Musée',
                'Café' => 'Café'
            ],
        ])->
        add('info',TextareaType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Information ")))->
        add('nom',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Nom Agence")))->
        add('Ajouter',SubmitType::class,array('label'=>'Ajouter','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->add('latitude',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Latitude ")))
            ->add('longitude',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Longitude ")));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Agence\userBundle\Entity\agence'
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'Agence_userbundle_agence';
    }



}
