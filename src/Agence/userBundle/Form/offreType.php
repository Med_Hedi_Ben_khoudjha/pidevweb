<?php

namespace Agence\userBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class offreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type',ChoiceType::class,[
            'choices'  => [
                'Choisissez' => null,
                'Restaurent' => 'Restaurent',
                'Hotel' => 'Hotel',
                'Musée' => 'Musée',
                'Café' => 'Café'
            ],
        ])
            ->add('info',TextareaType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Information")))
            ->add('libelle',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Libelle")))
            ->add('prix',TextType::class,array('required'=>false,'attr'=>array('class'=>'form-controle','placeholder'=>"Prix")))
            ->add('id_agence',EntityType::class,['class'=>'AgenceuserBundle:agence',
                'choice_label'=>'nom'])
            ->add('Ajouter',SubmitType::class,array('label'=>'Ajouter','attr'=>array('class'=>'btn btn-primary mt-3')))
        ;
    }


    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Agence\userBundle\Entity\offre'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'Agence_userbundle_offre';
    }


}
