<?php
/**
 * Created by PhpStorm.
 * User: MED
 * Date: 23/03/2019
 * Time: 05:15
 */

class AuthenticationController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @Route("/logout", name="logout")
     */
    public function loginAction(Request $request)
    {
        // Standard login stuff here
    }

    /**
     * @Route("/login_success", name="login_success")
     */
    public function postLoginRedirectAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('medmed_acc');
        } else if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('medadmin_homepage');
        } else {
            return $this->redirectToRoute("index");
        }
    }
}