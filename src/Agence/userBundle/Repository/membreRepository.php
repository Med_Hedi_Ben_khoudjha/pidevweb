<?php
/**
 * Created by PhpStorm.
 * User: MED
 * Date: 29/03/2019
 * Time: 02:30
 */

namespace Agence\userBundle\Repository;
use Doctrine\ORM\EntityRepository;



class membreRepository extends EntityRepository
{
    public function findByEmail($email){


        $query = $this->createQuery(
            'SELECT p
    FROM Agence\userBundle:membre p
    WHERE p.email LIKE :price'
        )->setParameter($email);

        return $query->getResult();

    }
}