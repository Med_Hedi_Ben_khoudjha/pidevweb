<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 27/04/2019
 * Time: 16:16
 */

namespace EspritApiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Loisir;
use Symfony\Component\HttpFoundation\Request;
use LoisirBundle\Form\LoisirType;





class LoisirApiController extends Controller
{

    public function allAction()
    {



        $Loisirs =$this->getDoctrine()->getManager()
            ->getRepository('AppBundle:Loisir')->findAll();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $serializer =new Serializer(array(new DateTimeNormalizer(),$normalizer));
        $formatted =$serializer->normalize($Loisirs);
        return new JsonResponse($formatted);


    }
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $loisir= new Loisir();
            $em = $this->getDoctrine()->getManager();
            $loisir->setCreator($this->getUser());
            $loisir->setDescription($request->get('description'));
            $loisir->setAdresse($request->get('adresse'));
            $loisir->setTitle($request->get('title'));
            $loisir->setPhoto($request->get('photo'));
            $loisir->setPostdate(new \DateTime('now'));
            $em->persist($loisir);
            $em->flush();
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($loisir);
            return new JsonResponse($formatted);


    }
}