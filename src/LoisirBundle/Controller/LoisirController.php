<?php
namespace LoisirBundle\Controller;
use AppBundle\Entity\Loisir;
use AppBundle\Entity\Loisircomment;
use AppBundle\Entity\LoisirLike;
use AppBundle\Repository\LoisirLikeRepository;
use AppBundle\Repository\LoisirRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use LoisirBundle\Form\LoisirType;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;




class LoisirController extends Controller
{
    public function addAction(Request $request)
    {
        $loisir = new Loisir();
        $form= $this->createForm(LoisirType::class, $loisir);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $file = $loisir->getPhoto();
            $filename= md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('photos_directory_loisir'), $filename);
            $loisir->setPhoto($filename);
            $loisir->setCreator($this->getUser());
            $loisir->setPostdate(new \DateTime('now'));
            $em->persist($loisir);
            $em->flush();
            $this->addFlash('info', 'Created Successfully !');
        }
        return $this->render('@Loisir/loisir/add.html.twig', array(
            "Form"=> $form->createView()
        ));
    }
    public function listloisirAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $loisirs=$em->getRepository('AppBundle:Loisir')->findAll();
        return $this->render('@Loisir/loisir/list.html.twig', array(
            "loisirs" =>$loisirs
        ));
    }
    public function listloisiraAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $loisirs=$em->getRepository('AppBundle:Loisir')->findAll();
        return $this->render('@Loisir/loisir/listA.html.twig', array(
            "loisirs" =>$loisirs
        ));
    }
    public function updateloisirAction(Request $request, $id)
    {
        $em=$this->getDoctrine()->getManager();
        $p= $em->getRepository('AppBundle:Loisir')->find($id);
        $form=$this->createForm(LoisirType::class,$p);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $file = $p->getPhoto();
            $filename= md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('photos_directory_loisir'), $filename);
            $p->setPhoto($filename);
            $p->setPostdate(new \DateTime('now'));
            $em= $this->getDoctrine()->getManager();
            $em->persist($p);
            $em->flush();
            return $this->redirectToRoute('list_loisir');
        }
        return $this->render('@Loisir/loisir/update.html.twig', array(
            "form"=> $form->createView()
        ));
    }
    public function deleteloisirAction(Request $request)
    {
        $id = $request->get('id');
        $em= $this->getDoctrine()->getManager();
        $Loisir=$em->getRepository('AppBundle:Loisir')->find($id);
        $em->remove($Loisir);
        $em->flush();
        return $this->redirectToRoute('list_loisir');
    }
    public function showdetailedAction($id)
    {
        $em= $this->getDoctrine()->getManager();
        $p=$em->getRepository('AppBundle:Loisir')->find($id);
        return $this->render('@Loisir/Loisir/detailedloisir.html.twig', array(
            'title'=>$p->getTitle(),
            'date'=>$p->getPostdate(),
            'photo'=>$p->getPhoto(),
            'descripion'=>$p->getDescription(),
            'loisirs'=>$p,
            'comments'=>$p,
            'id'=>$p->getId()
        ));
    }
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $requestString = $request->get('q');
        $loisirs =  $em->getRepository('AppBundle:Loisir')->findEntitiesByString($requestString);
        if(!$loisirs) {
            $result['loisirs']['error'] = "Post Not found :( ";
        } else {
            $result['loisirs'] = $this->getRealEntities($loisirs);
        }
        return new Response(json_encode($result));
    }
    public function getRealEntities($loisirs){
        foreach ($loisirs as $loisirs){
            $realEntities[$loisirs->getId()] = [$loisirs->getPhoto(),$loisirs->getTitle()];
        }
        return $realEntities;
    }
    public function addCommentAction(Request $request, UserInterface $user)
    {
        //if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
        //   return new RedirectResponse('/login');
        //}
        //$this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'unable to access this page.');
        $ref = $request->headers->get('referer');
        $loisir = $this->getDoctrine()
            ->getRepository(Loisir::class)
            ->findLoisirByid($request->request->get('loisir_id'));
        $comment = new Loisircomment();
        $comment->setUser($user);
        $comment->setLoisir($loisir);
        $comment->setContent($request->request->get('comment'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($comment);
        $em->flush();
        $this->addFlash(
            'info', 'Comment published !.'
        );
        return $this->redirect($ref);
    }
    public function deleteCommentAction(Request $request)
    {
        $id = $request->get('id');
        $em= $this->getDoctrine()->getManager();
        $comment=$em->getRepository('AppBundle:Loisircomment')->find($id);
        $em->remove($comment);
        $em->flush();
        return $this->redirectToRoute('list_loisir');
    }
    public function listcommentaireadAction()
    {
        //Créer une instance de l'Entity manager
        $em = $this->getDoctrine()->getManager();

        $commentaires = $em->getRepository("AppBundle:Loisircomment")
            ->findAll();
        return $this->render('@Annonce/Admin/listcommentaireannonce.html.twig'
            ,array(
                "commentaires"=>$commentaires

            ));
    }

    /**
     * @param Loisir $loisir
     * @param ObjectManager $manager
     * @param LoisirLikeRepository $repository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//liker ou disliker un article
    public function likeAction(
        Loisir $Loisir,  ObjectManager $ObjectManager
        ,LoisirLikeRepository $LoisirLikeRepository

    ) {


        $user = $this->getUser();
        if (!$user) return $this->json([
            'code' => 403,
            'message' => 'Unauthorized'
        ], 403);
        if ($Loisir->isLikeByUser($user)) {
            $like = $LoisirLikeRepository->findOneBy([
                'loisir' => $Loisir,
                'user' => $user
            ]);
            $ObjectManager->remove($like);
            $ObjectManager->flush();
            return $this->json([
                'code' => 200,
                'message' => 'Like supprimeé',
                'likes' => $LoisirLikeRepository->count(['Loisir' => $Loisir])
            ], 200);
        }
        $like = new LoisirLike();
        $like->setLoisir($Loisir);
        $like->setUser($user);
        $ObjectManager->persist($like);
        $ObjectManager->flush();
        return $this->json([
            'code' => 200,
            'message' => 'Like bien ajouté',
            'likes' => $LoisirLikeRepository->count(['Loisir' => $Loisir])
        ], 200);
    }


}